﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oopcv2
{
    internal class Bojovnik
    {
        public string jmeno { private set; get; }
        public int atk { private set; get; }
        public int def { private set; get; }
        public int hp { private set; get; }
        public int INT{ private set; get; }

        Random rnd = new Random();

        public Bojovnik(string jmeno, int atk, int def, int hp, int INT)
        {
            this.jmeno = jmeno;
            this.atk = atk;
            this.def = def;
            this.hp = hp;
            this.INT = INT;
        }

        public int genDmg()
        {
            int dmg = rnd.Next(1, atk);

            if(dmg <= atk/2)
            {
                dmg = dmg + 5;
            }

            return dmg;
        }

        public int genBlock()
        {
            int block = rnd.Next(1, def);
            return block;
        }

        public void stats()
        {
            Console.WriteLine("Jmeno: {0}\nHP: {1}\nAtk: {2}\nDef: {3}", this.jmeno, this.hp, this.atk, this.def);
        }

        public void takeDmg(int dmg)
        {
            int block = genBlock();

            if(block > dmg)
            {
                block = dmg;
                Console.WriteLine("Vyblokovany utok! ");
            }

            this.hp = this.hp - (dmg-block);
            Console.WriteLine("{0} blokuje {1} poskozeni", this.jmeno, block);
        }
        
        public int generateINT()
        {
            return this.rnd.Next(this.INT,);
        }
    }
}
