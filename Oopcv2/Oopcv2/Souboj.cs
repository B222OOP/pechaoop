﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

//git add.
//git commit -m "..."
//git push

namespace Oopcv2
{
    internal class Souboj
    {
        public Bojovnik boj1 { get; private set; }
        public Bojovnik boj2 { get; private set; }

        public Souboj(Bojovnik boj1, Bojovnik boj2)
        {
            this.boj1 = boj1;
            this.boj2 = boj2;
        }

        public void bitva()
        {
            int kolo = 1;
            Console.WriteLine("\n\n{0} VS. {1}", boj1.jmeno, boj2.jmeno);
            Console.ReadKey();

            while(boj1.hp > 0 && boj2.hp > 0)
            {
                Console.WriteLine("\n************* {0}. kolo *************", kolo);

                int dmg1 = boj1.genDmg();
                int dmg2 = boj2.genDmg();

                round(dmg1);
                round(dmg2);
                
                kolo++;

            }
            
        }
        public void vitez(Bojovnik bojovnik)
        {
            Console.WriteLine("--------------- {0} vitezi! ---------------", bojovnik.jmeno);
        }

        public void round(int damage)
        {

            Bojovnik utocnik = kdoUtoci();

            Console.WriteLine("{0} dava {1} {2} poskozeni", utocnik.jmeno, boj2.jmeno, damage);
            boj2.takeDmg(damage);
            Console.WriteLine("{0} zbyva {1} HP", boj2.jmeno, boj2.hp);

            if (boj2.hp < 0)
            {
                vitez(boj1);
                return;
            }

            if (boj1.hp < 0)
            {
                vitez(boj2);
                return;
            }

            Console.ReadKey();
        }

        public Bojovnik kdoUtoci()
        {

            int INT1 = this.boj1.generateINT;
            int INT2 = this.boj2.generateINT;
            if (INT1-INT2 > (INT1+INT2)/2)
            {
                
                return boj1;
            }
            else
            {
                return boj2;
            }

        }
    }
}
