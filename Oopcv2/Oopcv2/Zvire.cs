﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oopcv2
{
    public class Zvire
    {
        public string jmeno { get; private set; }
        private int vek { get; set; }
        private string typ { get; set; }

        // Konstruktor
        public Zvire(string jmeno, int vek, string typ)
        {
            this.jmeno = jmeno;
            this.vek = vek;
            this.typ = typ;
        }

        // Metody
        public void vypisVek()
        {
            Console.WriteLine("Zvireti " + this.jmeno + " je " + this.vek);
        }

        public void narozeniny()
        {
            this.vek++;
            Console.WriteLine(this.jmeno + " ma " + this.vek + ". narozeniny.");
            Console.WriteLine("\nZvire {0} ma {1}. narozeniny. Vsechno nejlepsi!", this.jmeno, this.vek);
        }

        public int vratVekPlus(int plus)
        {
            return this.vek + plus;
        }


    }
}
