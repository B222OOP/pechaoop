﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oopcv2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Zvire mojezvire = new Zvire("Karel", 3, "pes");
            //Console.WriteLine(mojezvire.jmeno);
            //mojezvire.vypisVek();
            //mojezvire.narozeniny();

            //int test = mojezvire.vratVekPlus(5);
            //Console.WriteLine("Ted mi to vratilo " + test);

            Bojovnik bojovnik1 = new Bojovnik("Zizka", 25, 12, 80, 12);
            bojovnik1.stats();
            //Console.WriteLine("{0} utoci za {1} !",bojovnik1.jmeno,bojovnik1.genDmg());

            Console.WriteLine();
            Bojovnik bojovnik2 = new Bojovnik("Honza", 18, 9, 60, 20);
            bojovnik2.stats();

            Souboj souboj = new Souboj(bojovnik1, bojovnik2);
            souboj.bitva();
            

            Console.ReadKey();
        }
    }
}
