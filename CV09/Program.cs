﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Pes p = new Pes("Max", 2, 4, "Pug", "Bila");
            Zvire z = new Zvire("Vasek", 3, 0, "Modra");

            Console.WriteLine(p.skrek());
            p.tryMethods();
            Console.WriteLine(p.barva + "\n");

            Console.WriteLine(z.skrek());
            Console.WriteLine(z.getJmeno());
            // z.tryMethods();
            Console.WriteLine(z.barva);

            Console.ReadKey();
        }
    }
}
