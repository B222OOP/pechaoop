﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV09
{
    public class Pes:Zvire // dedeni z class Zvire
    {
        public string plemeno;

        public Pes(string jmeno, int vek, int nohy, string plemeno, string barva):base(jmeno, vek, nohy, barva)
        {
            this.plemeno = plemeno;
        }

        public void tryMethods()
        {
            Console.WriteLine(getJmeno());

            //Console.WriteLine(getVek()); // Private metoda - pouze ve vlastni tride

            Console.WriteLine(getNohy()); // metoda je Protected - Muzeme ji volat ve tridach ktere ji dedi
        }

        public override string barva { get => base.barva + " Overrided"; set => base.barva = value; }
        public override string skrek()
        {
            return "Takhle steka pejsek";
        }

    }
}
