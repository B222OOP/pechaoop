﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV09
{
    public class Zvire
    {
        public string jmeno;
        public int vek;
        public int nohy;

        public virtual string barva { get; set; }

        public Zvire(string jmeno, int vek, int nohy, string barva)
        {
            this.jmeno = jmeno;
            this.vek = vek;
            this.nohy = nohy;
            this.barva = barva;
        }

        public virtual string skrek()
        {
            return "Takhle rve zvire.";
        }

        public string getJmeno()
        {
            return jmeno;
        }

        private int getVek()
        {
            return vek;
        }

        protected int getNohy()
        {
            return nohy;
        }
    }
}
