﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zapocet1
{
    internal class Room
    {
        public int bedCount { get; private set; }

        List<Bed> beds = new List<Bed>();

        public List<Patient> patients = new List<Patient> { };
        public int max_weight { get; private set; }

        public Room(int bedcount, int max_weight)
        {
            this.bedCount = bedcount;
            this.max_weight = max_weight;

            for(int i = 0; i < bedcount; i++)
            {
                beds.Add(new Bed("L" + (i + 1), max_weight));
            }
        }
        public void assignBedTo(Patient patient, int x)
        {
            if (patient.weight > max_weight)
            {
                Console.WriteLine("Prekrocena maximalni hmotnost luzka. Zvol jine");
                return;
            }
            if (beds[x].occupied == true)
            {
                Console.WriteLine("Luzko je obsazeno. Vyber jine.");
                return;
            }
            patients.Add(patient);
            beds[x].occupied = true;
            beds[x].occupiedBy = patient.Name + " " + patient.Surname;
        }

        public void removePatientFrom(int x)
        {
            beds[x].occupied = false;
            beds[x].occupiedBy = null;
            patients[x] = null;
        }

        public void getInfoOnRoom()
        {
            for(int i = 0; i < bedCount; i++)
            {
                if (beds[i].occupied == false)
                {
                    Console.WriteLine("\n***Nikdo není na luzku {0}***", beds[i].ID);
                }
                else
                {
                    Console.WriteLine("\nLuzko {0}", beds[i].ID);
                    beds[i].getInfoOnPatient(patients[i]);
                    // beds[i].getInfoOnPatient(patients[i-1]); // kdyz je jen jeden na pokoji
                }
            }
        }
    }
}
