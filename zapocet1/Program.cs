﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace zapocet1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Patient> Patients = new List<Patient>();
            List<Room> Rooms = new List<Room>();

            Patient pacient1 = new Patient("Tomas", "Jedno", 9110163535, 112, 178);
            Patient pacient2 = new Patient("Vladislav", "Jednoduchy", 0301041576, 80, 190);
            Patient pacient3 = new Patient("Marek", "Hodny", 7205288035, 95, 182);
            Patient pacient4 = new Patient("Honza", "Tichy", 7205288035, 95, 182);
            //Patients.Add(pacient1);
            //Patients.Add(pacient2);
            //Patients.Add(pacient3);

            Room pokoj1 = new Room(1, 110);
            Room pokoj2 = new Room(2, 115);
            Room pokoj3 = new Room(1, 85);
            Rooms.Add(pokoj1);
            Rooms.Add(pokoj2);
            Rooms.Add(pokoj3);

            pokoj1.assignBedTo(pacient1, 0); //posledni cislo argumentu je 'cislo pokoje-1'
            pokoj2.assignBedTo(pacient2, 1);
            pokoj3.assignBedTo(pacient3, 0);
            pokoj2.assignBedTo(pacient4, 0);

            pokoj2.getInfoOnRoom(); // muset doladit, funguje jen kdyz jsou v pokoji dva a vic

            Console.ReadKey();
        }
    }
}
