﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zapocet1
{
    internal class Patient
    {
        public string Name { private set; get; }
        public string Surname { private set; get; }
        public long rc { private set; get; }
        public int weight { private set; get; }
        public int height { private set; get; }
        public bool isPatient { private set; get; }

        public Patient(string name, string surname, long rc, int weight, int height)
        {
            this.Name = name;
            this.Surname = surname;
            this.rc = rc;
            this.weight = weight;
            this.height = height;
            this.isPatient = false;
        }

        public void weightLoss(int loss)
        {
            weight = weight - loss;
        }

        public void weightGain(int gain)
        {
            weight = weight + gain;
        }
        
        public float getBmi()
        {
            float heightInM = height / 100;
            float bmi = weight / (heightInM * heightInM);
            return bmi;
        }

        //public void asPatient()
        //{
        //    float bmi = getBmi();

        //    if (bmi < 18.5 || bmi > 25)
        //    {
        //        isPatient = true;
        //    }
        //    else isPatient = false;
        //}
        public void getInfo()
        {
            float bmi = getBmi();

            Console.WriteLine("\nJmeno: {0} {1} " +
                "\nRodne cislo: {2}" +
                "\nVyska: {3} cm" +
                "\nVaha: {4} kg" +
                "\nBMI: {5}" +
                "\n------", Name, Surname, rc, height, weight, bmi);
        }

    }
}
