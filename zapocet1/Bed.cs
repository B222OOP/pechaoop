﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace zapocet1
{
    internal class Bed
    {
        public string ID { private set; get; }
        public int max_weight { private set; get; }
        public bool occupied { set; get; }
        public string occupiedBy { set; get; }


        public Bed(string id, int max_weight)
        {
            this.ID = id;
            this.max_weight = max_weight;
            this.occupied = false;
        }


        public void getInfoOnPatient(Patient patient)
        {
            float heightInM = patient.height / 100;
            float bmi = patient.weight / (heightInM * heightInM);

            Console.WriteLine("\nJmeno: {0} {1} " +
                "\nRodne cislo: {2}" +
                "\nVyska: {3} cm" +
                "\nVaha: {4} kg" +
                "\nBMI: {5}" +
                "\n------", patient.Name, patient.Surname, patient.rc, patient.height, patient.weight, bmi);
        }
    }
}
