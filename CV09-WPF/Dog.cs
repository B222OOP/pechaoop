﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV09_WPF
{
    internal class Dog
    {
        public string name;
        public int age;
        public string breed;

        public Dog(string name, int age, string breed)
        {
            this.name = name;
            this.age = age;
            this.breed = breed;
        }
    }
}
