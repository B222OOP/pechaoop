﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV09_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            List<string> listStringu = new List<string>(new string[] 
            { "Jedna", "Dva", "Tri", "Ctyri", "Pet", "Sest", "Sedm", "Osm", "Devet", "Deset" });

            foreach (var o in listStringu) { lBox.Items.Add(o); }


        }

        private void btnDel_Data_Click(object sender, RoutedEventArgs e)
        {
            
            //for (int i = lBox.SelectedItems.Count - 1; i >= 0; i--)
            //{
            //    lBox.Items.RemoveAt();
            //}
            lBox.Items.Remove(lBox.SelectedItem);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            lBox.Items.Add(txtBx.Text);
            txtBx.Text = null;
        }

        private void btnDog_Click(object sender, RoutedEventArgs e)
        {
            Window1 subWindow = new Window1();
            subWindow.ShowDialog();
            Dog pes = new Dog(subWindow.txtBxName.Text, int.Parse(subWindow.txtBxAge.Text), subWindow.txtBxBreed.Text);
            lbl.Content = subWindow.txtBxName.Text + ", " + int.Parse(subWindow.txtBxAge.Text) + ", " + subWindow.txtBxBreed.Text;
            lBox.Items.Add(lbl.Content);
        }
    }
}
