﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
namespace PongGame
{
    internal class Ball
    {
        public Ellipse  ball { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int x { get; set; }
        public int y { get; set; }

        public Ball(int width, int height, int x, int y, Canvas cnv)
        {
            ball = new Ellipse();
            ball.Width = width;
            ball.Height = height;
            ball.Stroke = Brushes.LimeGreen;
            ball.Fill = Brushes.Black;
            ball.StrokeThickness = 2;

            cnv.Children.Add(ball);
            Canvas.SetLeft(ball, x);
            Canvas.SetTop(ball, y);
        }
    }
}
