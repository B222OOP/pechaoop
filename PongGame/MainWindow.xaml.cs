﻿using DB;
using MySqlConnector;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PongGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        player playerL;

        player playerR;

        Ball pongBall;

        double speedx = 0;

        double speedy = 0;

        double startSpeedx = 3;

        double startSpeedy;

        int scoreLeft;

        int scoreRight;

        int roundCount;

        bool started;

        Random r = new Random();

        DispatcherTimer ballTimer = new DispatcherTimer();
        DispatcherTimer cooldownTimer = new DispatcherTimer();

        int cd = 5;

        private DB_connect dbconn;
        private MySqlDataReader reader;

        public MainWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            started = false;
            ballTimer.Tick += animate;
            ballTimer.Interval = new TimeSpan(0, 0, 0, 0, 15);

            cooldownTimer.Interval = TimeSpan.FromSeconds(1);
            cooldownTimer.Tick += Timer_Tick;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {

            if (nameL.Text == "" || nameR.Text == "")
            {
                playerL = new player(0, (int)cnv.ActualHeight / 2, 20, 100, cnv);
                playerR = new player((int)cnv.ActualWidth - 20, (int)cnv.ActualHeight / 2, 20, 100, cnv);
            }
            else
            {
                playerL = new player(nameL.Text.ToString(), 0, (int)cnv.ActualHeight / 2, 20, 100, cnv);
                playerR = new player(nameL.Text.ToString(), (int)cnv.ActualWidth - 20, (int)cnv.ActualHeight / 2, 20, 100, cnv);
            }

            pongBall = new Ball(15, 15, (int)cnv.ActualWidth / 2, (int)cnv.ActualHeight / 2, cnv);

            roundCount = int.Parse(txtBxRounds.Text);

            Line halfLine = new Line();
            halfLine.X1 = cnv.ActualWidth / 2;
            halfLine.Y1 = 0;
            halfLine.X2 = halfLine.X1;
            halfLine.Y2 = cnv.ActualHeight;
            halfLine.StrokeThickness = 2;
            halfLine.Stroke = Brushes.White;
            grid.Children.Add(halfLine);

            btnHistory.Visibility = Visibility.Hidden;
            btnStart.Visibility = Visibility.Hidden;

            txtNum.Visibility = Visibility.Hidden;
            txtBxRounds.Visibility = Visibility.Hidden;
            info.Visibility = Visibility.Hidden;

            scoreL.Visibility = Visibility.Visible;
            scoreR.Visibility = Visibility.Visible;

            nameL.IsReadOnly = true;
            nameR.IsReadOnly = true;

            started = true;

            match();
        }

        private void goal()
        {
            speedx = 0;
            speedy = 0;

            ballTimer.Stop();

            Canvas.SetLeft(pongBall.ball, (int)cnv.ActualWidth / 2);
            Canvas.SetTop(pongBall.ball, (int)cnv.ActualHeight / 2);

            if (int.Parse(scoreL.Content.ToString()) + int.Parse(scoreR.Content.ToString()) == roundCount)
            {
                if (int.Parse(scoreL.Content.ToString()) > int.Parse(scoreR.Content.ToString()))
                {
                    playerLWin.Visibility = Visibility.Visible;
                    player winner = playerL;
                    btnRestart.Visibility = Visibility.Visible;
                    return;
                }
                else if (int.Parse(scoreL.Content.ToString()) < int.Parse(scoreR.Content.ToString()))
                {
                    playerRWin.Visibility = Visibility.Visible;
                    player winner = playerR;
                    btnRestart.Visibility = Visibility.Visible;
                    return;
                }
                else
                {
                    lblDraw.Visibility = Visibility.Visible;
                    btnRestart.Visibility = Visibility.Visible;
                    return;
                }

            }

            Countdown();
            match();

        }

        void Countdown()
        {
            cd = 5;

            CDTimer.Visibility = Visibility.Visible;
            pongBall.ball.Visibility = Visibility.Hidden;

            cooldownTimer.Start();

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            CDTimer.Content = cd;

            if (CDTimer.Content.ToString() == "0")
            {
                cooldownTimer.Stop();

                pongBall.ball.Visibility = Visibility.Visible;
                CDTimer.Visibility = Visibility.Hidden;

                ballTimer.Start();
            }
            cd--;
        }

        private void animate(object sender, EventArgs e)
        {
            if (Canvas.GetLeft(pongBall.ball) >= cnv.ActualWidth)
            {
                scoreLeft++;
                scoreL.Content = scoreLeft;
                goal();
            }
            if (Canvas.GetLeft(pongBall.ball) <= 0)
            {
                scoreRight++;
                scoreR.Content = scoreRight;
                goal();
            }

            if (Canvas.GetTop(pongBall.ball) <= 0)
            {
                speedy = -speedy;
            }
            if (Canvas.GetTop(pongBall.ball) + pongBall.ball.ActualHeight >= cnv.ActualHeight)
            {
                speedy = -speedy;
            }

            //kolize s hracem vlevo
            if (Canvas.GetLeft(pongBall.ball) <= playerL.playerRect.ActualWidth
                && Canvas.GetTop(pongBall.ball) >= Canvas.GetTop(playerL.playerRect)
                && Canvas.GetTop(pongBall.ball) + pongBall.ball.ActualHeight <= Canvas.GetTop(playerL.playerRect) + playerL.playerRect.ActualHeight)
            {
                speedx = -speedx;
                speedx *= 1.05;
                speedy *= 1.05;
            }

            //kolize s hracem vpravo
            if (Canvas.GetLeft(pongBall.ball) + pongBall.ball.ActualWidth >= cnv.ActualWidth - playerR.playerRect.ActualWidth
                && Canvas.GetTop(pongBall.ball) + pongBall.ball.ActualWidth >= Canvas.GetTop(playerR.playerRect)
                && Canvas.GetTop(pongBall.ball) <= Canvas.GetTop(playerR.playerRect) + playerR.playerRect.ActualHeight)
            {
                speedx = -speedx;
                speedx *= 1.05;
                speedy *= 1.05;
            }

            Canvas.SetLeft(pongBall.ball, Canvas.GetLeft(pongBall.ball) + speedx);
            Canvas.SetTop(pongBall.ball, Canvas.GetTop(pongBall.ball) + speedy);
        }

        private void match()
        {
            if (scoreL.Content.ToString() == "0" && scoreR.Content.ToString() == "0")
            {
                ballTimer.Start();
            }

            int firstTurn = r.Next(2);

            int speedy1 = r.Next(3, 6);
            int speedy2 = -r.Next(3, 6);

            startSpeedy = r.Next(0, 2) == 1 ? speedy1 : speedy2;
            speedy = startSpeedy;

            if (firstTurn == 1)
            {
                speedx = 3;
            }
            else if (firstTurn == 0)
            {
                speedx = -3;
            }

        }

        private void window_KeyDown(object sender, KeyEventArgs e)
        {
            if (started)
            {
                if (e.Key == Key.S)
                {
                    if (Canvas.GetTop(playerL.playerRect) + playerL.playerRect.ActualHeight <= cnv.ActualHeight)
                    {
                        Canvas.SetTop(playerL.playerRect, Canvas.GetTop(playerL.playerRect) + 15);
                    }
                }

                else if (e.Key == Key.W)
                {
                    if (Canvas.GetTop(playerL.playerRect) >= 0.0)
                    {
                        Canvas.SetTop(playerL.playerRect, Canvas.GetTop(playerL.playerRect) - 15);
                    }
                }

                if (e.Key == Key.Down)
                {
                    if (Canvas.GetTop(playerR.playerRect) + playerR.playerRect.ActualHeight <= cnv.ActualHeight)
                    {
                        Canvas.SetTop(playerR.playerRect, Canvas.GetTop(playerR.playerRect) + 15);
                    }
                }

                else if (e.Key == Key.Up)
                {
                    if (Canvas.GetTop(playerR.playerRect) > 0.0)
                    {
                        Canvas.SetTop(playerR.playerRect, Canvas.GetTop(playerR.playerRect) - 15);
                    }
                }
            }

        }

        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            if (nameL.Text == "" || nameR.Text == "")
            {
                this.Close();
            }
            else
            {
                sendToDatabase();
                this.Close();
            }
        }

        private void sendToDatabase()
        {
            dbconn.Insert(String.Format("INSERT INTO PongHistory (Player1,Player2,Score1,Score2) " +
                                       "VALUES ('{0}','{1}','{2}','{3}')",
                                       nameL.Text.ToString(), nameR.Text.ToString(), int.Parse(scoreL.Content.ToString()), int.Parse(scoreR.Content.ToString())));
        }

        private void btnHistory_Click(object sender, RoutedEventArgs e)
        {
            HistoryWindow historyWindow = new HistoryWindow();
            historyWindow.ShowDialog();


        }
    }
}
