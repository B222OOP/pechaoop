﻿using DB;
using MySqlConnector;
using System;
using System.Windows;
using static System.Formats.Asn1.AsnWriter;

namespace PongGame
{
    /// <summary>
    /// Interaction logic for HistoryWindow.xaml
    /// </summary>
    public partial class HistoryWindow : Window
    {

        private DB_connect dbconn;
        private MySqlDataReader reader;
        public HistoryWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();

            string sel = "SELECT * FROM PongHistory";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listBxHistory.Items.Add(String.Format("{0} vs. {1} ( {2} : {3} )", 
                    reader.GetString(1), 
                    reader.GetString(2), 
                    reader.GetInt32(3),
                    reader.GetInt32(4)));
            }
            this.dbconn.CloseConn();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            string selectedMatch = (string)listBxHistory.SelectedItem;
            string[] name = selectedMatch.Split(" ");

            listBxHistory.Items.Remove(listBxHistory.SelectedItem);
            dbconn.Delete(String.Format("DELETE FROM PongHistory WHERE Player1='{0}' && Player2='{1}'", name[0], name[2]));
        }
    }
}
