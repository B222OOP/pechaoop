﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xaml.Schema;

namespace PongGame
{
    internal class player
    {
        public System.Windows.Shapes.Rectangle playerRect { get; set; }
        public string name { get; set; }

        public player(int x, int y, int width, int height, Canvas cnv)
        {
            playerRect = new System.Windows.Shapes.Rectangle();
            playerRect.Width = width;
            playerRect.Height = height;
            playerRect.Stroke = Brushes.LimeGreen;
            playerRect.Fill = Brushes.Black;
            playerRect.StrokeThickness = 2;
            

            cnv.Children.Add(playerRect);
            Canvas.SetTop(playerRect, y);
            Canvas.SetLeft(playerRect, x);

        }
        public player(string name, int x, int y, int width, int height, Canvas cnv)
        {
            this.name = name;
            playerRect = new System.Windows.Shapes.Rectangle();
            playerRect.Width = width;
            playerRect.Height = height;
            playerRect.Stroke = Brushes.LimeGreen;
            playerRect.Fill = Brushes.Black;
            playerRect.StrokeThickness = 2;


            cnv.Children.Add(playerRect);
            Canvas.SetTop(playerRect, y);
            Canvas.SetLeft(playerRect, x);

        }
    }
}
