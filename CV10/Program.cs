﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV10
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //int[] pole = { 0, 1, 2, 3, 4 };
            //int[] pole2 = (int[])pole.Clone();
            //pole2[1] = 4;

            //Console.WriteLine(pole[1]);


            //ArrayList listik = new ArrayList();
            //listik.Add(1);
            //listik.Add("Ahoj");
            //listik.Add(true);

            //foreach (var i in listik)
            //{
            //    Console.WriteLine(i);
            //}

            //List<int> intgenericList = new List<int>();

            //GenerickaTrida<int> trida1 = new GenerickaTrida<int>(12);
            //trida1.vypis();
            //GenerickaTrida<double> trida2 = new GenerickaTrida<double>(5.2);
            //trida2.vypis();

            //string path = @"C:\Users\honzi\Documents\Gitlab\pechaoop\CV10\Data\";
            //string filename = "Muj Soubor1.txt";

            //using (StreamWriter sw = new StreamWriter(Path.Combine(path,filename),true))
            //{
            //    sw.WriteLine("Ahoj world");
            //}

            //using (StreamReader sr = new StreamReader(Path.Combine(path,filename)))
            //{
            //    Console.WriteLine(sr.ReadToEnd());
            //}

            //Dictionary<string, string> slovnik = new Dictionary<string, string>();
            //slovnik.Add("klic", "Toto je hodnota");
            //Console.WriteLine(slovnik["klic"]);
            //Dictionary<dynamic, dynamic> slovnik2 = new Dictionary<dynamic, dynamic>();
            //slovnik2.Add(3, 4);
            //slovnik2.Add(7.4, "ahoj");
            //slovnik2.Add("test", 1);


            Dictionary<dynamic,dynamic> dict1 = new Dictionary<dynamic,dynamic>();

            for (int i = 0; i < 10; i++)
            {
                dict1.Add(i,i);
            }

            string path = @"C:\Users\honzi\Documents\Gitlab\pechaoop\CV10\Data\";
            string filename = "Slovnik.txt";
            using (StreamWriter sw = new StreamWriter(Path.Combine(path, filename)))
            {
                for(int i = 0; i < dict1.Count;i++)
                {
                    sw.WriteLine("{0} {1}", i, dict1[i]);
                }
            }

            Dictionary<dynamic,dynamic> dict2 = new Dictionary<dynamic,dynamic>();

            using(StreamReader sr = new StreamReader(Path.Combine(path, filename)))
            {
                for (int i = 0; i < File.ReadLines(Path.Combine(path, filename)).Count(); i++)
                {
                    string line = sr.ReadLine();
                    string[] strings = line.Split(' ');
                    dict2.Add((dynamic)strings[0], (dynamic)strings[1]);
                }
            }

            for (int i = 0; i < dict2.Count; i++)
            {
                Console.WriteLine(dict2[i.ToString()]);
            }
            

            Console.ReadKey();
        }
    }

    public class GenerickaTrida <T>
    {
        private T variable;


        public GenerickaTrida(T variable)
        {
            this.variable = variable;
        }

        public void vypis()
        {
            Console.WriteLine(String.Format("Promenna typu:{0} ma hodnotu {1}",variable, variable));
        }
    }
}
