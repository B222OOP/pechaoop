﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CV11
{
    internal class Program
    {
        //Ukladani vytvareni
        static void Main(string[] args)
        {
            /*
            XDocument xdoc = new XDocument(
                new XElement("patients",
                        new XElement("patient",
                            new XAttribute("name", "Thomas"),
                            new XAttribute("surname","Jedno"),
                                new XElement("Mereni","neco"))));

            string filepath = @"C:\Users\honzi\Documents\Gitlab\pechaoop\CV11\test.xml";
            xdoc.Save(filepath);

            //Nacitani
            XDocument xdoc1 = XDocument.Load(filepath);

            foreach(var node in xdoc1.DescendantNodes())
            {
                Console.WriteLine(
                    String.Format("V promenne se nachazi: {0} a je to typu: {1}", node, node.GetType()));
                if (node is XText)
                {

                }
            }
            */

            //DATABAZE

            List<Pacient> pacientList = new List<Pacient>();
            DB_connect dbconn;
            MySqlDataReader reader;
            dbconn = new DB_connect();

            reader = dbconn.Select("Select jmeno,prijmeni,vek FROM Pacient");
            while (reader.Read())
            {
                //Console.WriteLine(String.Format("{0} {1}", reader.GetInt32(0), reader.GetString(1)));
                //Vytvorit pacienty a dat je do listu
                pacientList.Add(new Pacient(reader.GetString(0), reader.GetString(1), reader.GetInt32(2)));
            }
            //Console.WriteLine("");
            dbconn.CloseConn();

            //foreachem vypsat pacienty
            foreach(Pacient pacient in pacientList)
            {
                Console.WriteLine(String.Format("Jmeno: {0} {1}\t Vek: {2}",pacient.jmeno, pacient.prijmeni, pacient.vek));
            }

            dbconn.Insert("INSERT INTO Pacient (jmeno,prijmeni,vek,adresa_id) " +
                "VALUES ('Senor','Madrazzo','56','1')");

            Console.ReadKey();
        }
    }
}
