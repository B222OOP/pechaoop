﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV11
{
    internal class Pacient
    {
        public string jmeno { get; private set; }
        public string prijmeni { get; private set; }
        public int vek { get; private set; }

        public Pacient(string jmeno, string prijmeni, int vek)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.vek = vek;

        }
    }
}
