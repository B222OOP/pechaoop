﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CV11_2
{
    /// <summary>
    /// Interaction logic for UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        
        public UpdateWindow()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtBxName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBxName.IsFocused)
            {
                txtBxName.Background = Brushes.Red;
            }
        }

        private void txtBxSurname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBxSurname.IsFocused)
            {
                txtBxSurname.Background = Brushes.Red;
            }
        }

        private void txtBxAge_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBxAge.IsFocused)
            {
            txtBxAge.Background = Brushes.Red;
            }
        }
    }
}
