﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV11_2
{
    public class Pacient
    {
        public int id { get; private set; }
        public string jmeno { get; private set; }
        public string prijmeni { get; private set; }
        public int vek { get; private set; }

        public Pacient(int id,string jmeno, string prijmeni, int vek)
        {
            this.id = id;
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.vek = vek;

        }
    }
}
