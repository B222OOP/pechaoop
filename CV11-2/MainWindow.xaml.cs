﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV11_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Pacient> patientList = new List<Pacient>();
        private DB_connect dbconn;
        private MySqlDataReader reader;

        public MainWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh();
            
    }

    private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            PatientAdd patientAdd = new PatientAdd();
            patientAdd.ShowDialog();

            
            Pacient patient = new Pacient(patientListBox.Items.Count+1, patientAdd.txtName.Text.ToString(), patientAdd.txtSurname.Text.ToString(), int.Parse(patientAdd.txtAge.Text));
            int s;
            if (patientList.Count == 0)
            {
                s = 0;
            }
            else { s = patientList.ElementAt(^1).id; }
            patientListBox.Items.Add(String.Format("[{0}] {1} {2} {3}", (s+1), patientAdd.txtName.Text.ToString(), patientAdd.txtSurname.Text.ToString(), patientAdd.txtAge.Text.ToString()));
            dbconn.Insert(String.Format("INSERT INTO Patients (ID,name,surname,age) " +
                                        "VALUES ('{0}','{1}','{2}','{3}')",
                                        (s+1), patientAdd.txtName.Text.ToString(), patientAdd.txtSurname.Text.ToString(), int.Parse(patientAdd.txtAge.Text)));

            patientList.Add(patient);
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            string[] s = patientListBox.SelectedItem.ToString().Split(" ");
            Pacient pac = getByName(s[1], s[2]);
            if (pac != null)
            {
                dbconn.Delete(String.Format("DELETE FROM Patients WHERE id={0}", pac.id));
                refresh();
            }

        }
        public Pacient getByName(string name, string surname)
        {
            foreach (var o in patientList)
            {
                if (o.jmeno == name && o.prijmeni == surname)
                {
                    return o;
                }
            }
            return null;
        }

        public void refresh()
        {
            patientList.Clear();
            string sel = "SELECT * FROM Patients";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                patientList.Add(new Pacient(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3)
                    ));
            }
            this.dbconn.CloseConn();
            patientListBox.Items.Clear();
            foreach (var o in this.patientList)
            {
                patientListBox.Items.Add(String.Format("[{0}] {1} {2} {3}", o.id, o.jmeno, o.prijmeni, o.vek));
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            UpdateWindow updateWindow = new UpdateWindow();
            string[] s = patientListBox.SelectedItem.ToString().Split(" ");
            Pacient pac = getByName(s[1], s[2]);

            updateWindow.txtBxName.Text = pac.jmeno;
            updateWindow.txtBxSurname.Text = pac.prijmeni;
            updateWindow.txtBxAge.Text = pac.vek.ToString();

            updateWindow.ShowDialog();

            if (updateWindow.txtBxName.Text != pac.jmeno ||
                updateWindow.txtBxSurname.Text != pac.prijmeni ||
                updateWindow.txtBxAge.Text != pac.vek.ToString())
            {
                dbconn.Update(String.Format("UPDATE Patients " +
                                            "SET name = '{0}', surname = '{1}',age = '{2}'" +
                                            " WHERE id={3}", updateWindow.txtBxName.Text, updateWindow.txtBxSurname.Text, updateWindow.txtBxAge.Text, pac.id));
                refresh();
            }

        }
    }
}
