﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV07_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {


            /*
            int x = int.Parse(tb1.Text);
            x++;

            tb1.Text = x.ToString();

            if(lbl.Content == "")
            {
                lbl.Content = "1";
            }

            lbl.Content += ", " + x.ToString();
            */
            //lbl.Content = tb1.Text;

            //tb1.Text = "";
            
            /*if (lbl.Content == "Ahoj svete")
            {
                lbl.Content = "Hello world";
                
            }
            else lbl.Content = "Ahoj svete";
            */
        }

        private void plus_Click(object sender, RoutedEventArgs e)
        {
            //int a = int.Parse(tblk.Text) + int.Parse(tb1.Text);
            tb1.Text = (int.Parse(tb1.Text) + int.Parse(tblk.Text)).ToString();
            tblk.Text = "";
        }

        private void minus_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = (int.Parse(tb1.Text) - int.Parse(tblk.Text)).ToString();
            tblk.Text = "";
        }

        private void nasobeni_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = (int.Parse(tb1.Text) * int.Parse(tblk.Text)).ToString();
            tblk.Text = "";
        }

        private void deleno_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = (int.Parse(tb1.Text) / int.Parse(tblk.Text)).ToString();
            tblk.Text = "";
        }

        private void procento_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = (int.Parse(tb1.Text) % int.Parse(tblk.Text)).ToString();
            tblk.Text = "";
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = 0.ToString();
            tblk.Text = "";
        }
    }
}
