﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace CV01
{
    internal class Program
    {
        /*
        static void vypis(string str)
        {
            Console.WriteLine(str);
        }

        static double soucet(double x, double y)
        {
            return x + y;
        }

        static double odecet(double x, double y)
        {
            return x - y;
        }

        static double deleni(double x, double y)
        {
            return x/y;
        }

        static double nasobeni(double x, double y)
        {
            return x * y;
        }

        static double prictiDva(double x)
        {
            return x + 2;
        } */

        static void napln(int[,] x)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] = i*x.GetLength(1)+j;
                    Console.Write("{0} ", x[i, j]);
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            int[,] x = new int[10, 10];
            napln(x);

            /*
            int[] x;
            x = new int[5];
            int xLen = x.GetLength(0);

            int[] y = new int[10];
            int yLen = y.GetLength(0);


            for (int i = 0; i < xLen; i++)
            {
                x[i] = i;
                Console.Write(x[i]);
            }

            Console.WriteLine();

            for (int j = 0; j < yLen; j++)
            {
                y[j] = j;
                Console.Write(y[j]);
            }*/


            //vypis("Ahoj z moji nove fce");
            //double a = 4;
            //double b = 3;
            //double c = 5;


            //Console.WriteLine("Muj krasny vzorecek vysel: {0}", deleni(odecet(soucet(a, b), nasobeni(c, a)), odecet(soucet(b, c), a)));

            /*
            int a = 5;
            double b = 2.3;
            bool pravda = true;
            char ch = 's';
            string str = "AhojSvete";

            Console.WriteLine("{0} je: {1}",a,pravda); //formatovany retezec
            Console.WriteLine("Hello World!");
            Console.WriteLine(b);
            
            //string strnew=Console.ReadLine();
            //Console.WriteLine("{0}, toto jsem napsal",strnew);

            double cislo = double.Parse(Console.ReadLine()); //parse na double, readline vraci string
            Console.WriteLine(cislo + 2);
            

            int x = 5;
            while(x>0)
            {
                Console.WriteLine("X je rovno {0}", x);
                x--;
            }

            x = 5;
            do
            {
                Console.WriteLine("X je rovno {0}", x);
                x--;
            } while (x > 0);


            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    if (j % 2 == 0)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write("/");
                    }
                }
                Console.WriteLine("");
            }
            */

            Console.ReadKey();
        }
    }
}
