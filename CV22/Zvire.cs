﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv02
{
    public class Zvire
    {
        //Parametry
        public string jmeno { get; private set; }
        private int vek { get; set; }
        private string typ { get; set; }

        //Konstruktor
        public Zvire(string jmeno, int vek, string typ)
        {
            this.jmeno = jmeno;
            this.vek = vek;
            this.typ = typ;
        }
        //Metody
        public void vypisVek()
        {
            //Console.WriteLine("Zvireti "+this.jmeno+" je:"+this.vek);
            Console.WriteLine("\nZvireti {0} je:{1}", this.jmeno, this.vek);
        }

        // Public metodu k narozeninam.
        public void oRokStarsi()
        {
            this.vek++;
            Console.WriteLine("Nas kamarad:{0} ma narozeniny, nyni mu je {1}", this.jmeno, this.vek);
        }

        public int vratVekPlus(int plus)
        {
            return this.vek + plus;
        }

    }
}