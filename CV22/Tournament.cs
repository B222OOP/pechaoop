﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv02
{
    internal class Tournament
    {
        public List<Fighter> fighters { get; private set; }
        
        List<Fighter> winners = new List<Fighter>();


        public Tournament(List<Fighter> fighters)
        {
            this.fighters = fighters;
        }

        public void CreateTournamentTree(List<Fighter> fighters)
        {
            while (this.fighters.Count != 1) {

                if (Math.Sqrt(fighters.Count) % 1 != 0)
                {
                    Console.WriteLine("Pocet bojovniku neni sudy. Vypni program stisknutim libovolne klavesy");
                    return;
                }

                for (int i = 0; i < fighters.Count; i += 2)
                {
                    Fight souboj = new Fight(fighters[i], fighters[i + 1]);

                    souboj.startFight();
                    winners.Add(souboj.returnWinner());


                }
                GetWinners();
            }
        }

        public void GetWinners()
        {
            if (winners.Count == 1)
            {
                Console.WriteLine("Vyhercem turnaje se stava: {0}", winners[0]);
            }

            this.fighters = winners;
            winners = new List<Fighter>();
        }
    }
}
