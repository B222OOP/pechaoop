﻿using DB;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPTest3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public string nickname;
        public MainWindow()
        {
            InitializeComponent();
            this.dbconn = new DB_connect();
        }

        private void logBtn_Click(object sender, RoutedEventArgs e)
        {
            nickname = nicknameBox.Text.ToString();
            string password = passwordBox.Text.ToString();
            string role;

            try
            {
                reader = this.dbconn.Select(String.Format("SELECT *  FROM Uzivatel WHERE Prezdivka='{0}' AND Heslo='{1}'", nickname, password));

                reader.Read();
                if (reader.GetInt32(3) == 1) { role = "Admin"; }
                else { role = "Zakaznik"; }

                //txtInfo.Text = String.Format("{0} {1} {2} {3} ", reader.GetString(0), reader.GetString(1), reader.GetString(2), role);
                txtInfo.Foreground = Brushes.DarkGreen;
                txtInfo.Text = "Uspesne prihlasen";

                if(role == "Admin")
                {
                    AdminWindow adminWindow = new AdminWindow();
                    adminWindow.ShowDialog();
                }
                else if(role == "Zakaznik")
                {
                    ZakaznikWindow zakaznikWindow = new ZakaznikWindow(nickname);
                    zakaznikWindow.ShowDialog();
                }
            }
            catch (InvalidOperationException) 
            {
                txtInfo.Foreground = Brushes.Red;
                txtInfo.Text = "Spatne udaje";
            }

            
        }
    }
}
