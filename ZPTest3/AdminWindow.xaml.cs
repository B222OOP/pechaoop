﻿using DB;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPTest3
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public string typSeznam;
        public AdminWindow()
        {
            InitializeComponent();
            this.dbconn = new DB_connect();
        }

        private void btnFilmy_Click(object sender, RoutedEventArgs e)
        {
            btnSmazat.Visibility = Visibility.Hidden;
            btnVytvorit.Visibility = Visibility.Hidden;

            seznam.Items.Clear();
            reader = dbconn.Select(String.Format("SELECT * FROM Film"));
            while (reader.Read()) {
                seznam.Items.Add(String.Format("{0}\t{1}\t{2}", reader.GetString(1), reader.GetString(2), reader.GetBoolean(3)));
            }
        }

        private void btnZakaznici_Click(object sender, RoutedEventArgs e)
        {
            btnSmazat.Visibility = Visibility.Visible;
            btnVytvorit.Visibility = Visibility.Visible;

            typSeznam = "Zakaznik";

            seznam.Items.Clear();
            reader = dbconn.Select(String.Format("Select * FROM Uzivatel"));
            while (reader.Read())
            {
                seznam.Items.Add(String.Format("{0}\t{1}", reader.GetString(0), reader.GetString(1)));
            }


        }

        private void btnSmazat_Click(object sender, RoutedEventArgs e)
        {

            if (typSeznam == "Zakaznik")
            {
                string selected = seznam.SelectedItem.ToString();
                string[] s = selected.Split("\t");
                dbconn.Delete(String.Format("DELETE FROM Uzivatel WHERE Jmeno='{0}' AND Prijmeni='{1}'", s[0], s[1]));
                seznam.SelectedItem = null;

                seznam.Items.Clear();
                reader = dbconn.Select(String.Format("Select * FROM Uzivatel"));
                while (reader.Read())
                {
                    seznam.Items.Add(String.Format("{0}\t{1}", reader.GetString(0), reader.GetString(1)));
                }
            }

            if (typSeznam == "Rezervace")
            {
                string selected = seznam.SelectedItem.ToString();
                string[] s = selected.Split("\t");
                dbconn.Delete(String.Format("DELETE Rezervace FROM Rezervace INNER JOIN Film ON Film.Film_ID=Rezervace.Film_ID" +
                    " INNER JOIN Uzivatel ON Uzivatel.Uzivatel_ID=Rezervace.Uzivatel_ID WHERE Uzivatel.Jmeno='{0}' AND Uzivatel.Prijmeni='{1}'", s[0], s[1]));
                seznam.SelectedItem = null;

                dbconn.Update(String.Format("UPDATE Film SET Rezervovany=0 WHERE Film.Nazev='{0}'", s[2]));

                seznam.Items.Clear();
                reader = dbconn.Select(String.Format("Select Uzivatel.Jmeno,Uzivatel.Prijmeni,Film.Nazev,Platnost_do FROM Rezervace INNER JOIN Film ON Film.Film_ID=Rezervace.Film_ID " +
                    "INNER JOIN Uzivatel ON Uzivatel.Uzivatel_ID=Rezervace.Uzivatel_ID"));
                while (reader.Read())
                {
                    seznam.Items.Add(String.Format("{0}\t{1}\t{2}\t{3}", reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3)));
                }
            }
        }

        private void btnVytvorit_Click(object sender, RoutedEventArgs e)
        {
            NovyUzivatel novyUzivatel = new NovyUzivatel();
            novyUzivatel.ShowDialog();

            string name = novyUzivatel.nameBox.Text;
            string surname = novyUzivatel.surnameBox.Text;
            string nickname = novyUzivatel.nicknameBox.Text;
            string password = novyUzivatel.passwordBox.Text;
            int role;

            if(novyUzivatel.roleBox.Text == "Admin")
            {
                role = 1;
            }
            else
            {
                role = 2;
            }

            dbconn.Insert(String.Format("INSERT INTO Uzivatel (Jmeno, Prijmeni, Prezdivka, Role_ID, Heslo)" +
                "VALUES ('{0}','{1}','{2}','{3}','{4}')", name, surname, nickname, role, password));

            seznam.Items.Clear();
            reader = dbconn.Select(String.Format("Select * FROM Uzivatel"));
            while (reader.Read())
            {
                seznam.Items.Add(String.Format("{0}\t{1}", reader.GetString(0), reader.GetString(1)));
            }
        }

        private void btnRezervace_Click(object sender, RoutedEventArgs e)
        {
            typSeznam = "Rezervace";

            btnVytvorit.Visibility = Visibility.Hidden;
            btnSmazat.Visibility = Visibility.Visible;

            seznam.Items.Clear();
            reader = dbconn.Select(String.Format("Select Uzivatel.Jmeno,Uzivatel.Prijmeni,Film.Nazev,Platnost_do FROM Rezervace INNER JOIN Film ON Film.Film_ID=Rezervace.Film_ID " +
                "INNER JOIN Uzivatel ON Uzivatel.Uzivatel_ID=Rezervace.Uzivatel_ID"));
            while (reader.Read())
            {
                seznam.Items.Add(String.Format("{0}\t{1}\t{2}\t{3}", reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3)));
            }
        }

        private void filterBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem cbi = (ComboBoxItem)filterBox.SelectedItem;
            string selectedText = cbi.Content.ToString();

            if (selectedText == "Jmeno")
            {
                seznam.Items.Clear();
                reader = dbconn.Select(String.Format("Select * FROM Uzivatel ORDER BY Jmeno ASC"));
                while (reader.Read())
                {
                    seznam.Items.Add(String.Format("{0}\t{1}", reader.GetString(0), reader.GetString(1)));
                }
            }

            if(selectedText == "Prijmeni")
            {
                seznam.Items.Clear();
                reader = dbconn.Select(String.Format("Select * FROM Uzivatel ORDER BY Prijmeni ASC"));
                while (reader.Read())
                {
                    seznam.Items.Add(String.Format("{0}\t{1}\t", reader.GetString(0), reader.GetString(1)));
                }
            }

            if(selectedText == "Prezdivka")
            {
                seznam.Items.Clear();
                reader = dbconn.Select(String.Format("Select * FROM Uzivatel ORDER BY Prezdivka ASC"));
                while (reader.Read())
                {
                    seznam.Items.Add(String.Format("{0}\t{1}\t{2}", reader.GetString(0), reader.GetString(1), reader.GetString(2)));
                }
            }

        }
    }
}
