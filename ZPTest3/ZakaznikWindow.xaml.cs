﻿using DB;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPTest3
{
    /// <summary>
    /// Interaction logic for ZakaznikWindow.xaml
    /// </summary>
    public partial class ZakaznikWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;

        string nickname;
        public ZakaznikWindow(string nickname)
        {
            InitializeComponent();
            this.dbconn = new DB_connect();
            this.nickname = nickname;
        }

        
        private void btnFilmy_Click(object sender, RoutedEventArgs e)
        {
            refreshFilmy();
        }

        private void refreshFilmy()
        {
            seznam.SelectedIndex = -1;
            seznam.Items.Clear();
            bool reserved;
            reader = dbconn.Select(String.Format("SELECT * FROM Film LEFT JOIN Rezervace ON Film.Film_ID=Rezervace.Film_ID"));
            while (reader.Read())
            {
                reserved = reader.GetBoolean(3);
                if (reserved)
                {
                    seznam.Items.Add(String.Format("{0}\t{1}\t{2}\t{3}", reader.GetString(1), reader.GetString(2), reader.GetBoolean(3), reader.GetDateTime(7)));
                }
                else
                {
                    seznam.Items.Add(String.Format("{0}\t{1}\t{2}", reader.GetString(1), reader.GetString(2), reader.GetBoolean(3)));
                }
            }
        }

        private void seznam_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (seznam.SelectedIndex != -1)
            {
                string selected = seznam.SelectedItem.ToString();
                string[] s = selected.Split("\t");

                if (s[2] == "True")
                {
                    dateRezervace.Visibility = Visibility.Hidden;
                    btnRezervace.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnRezervace.Visibility = Visibility.Visible;
                    dateRezervace.Visibility = Visibility.Visible;
                }
            }
        }

        private void btnRezervace_Click(object sender, RoutedEventArgs e)
        {
            if (seznam.SelectedIndex != -1)
            {
                string selected = seznam.SelectedItem.ToString();
                string[] s = selected.Split("\t");

                string date = dateRezervace.SelectedDate.Value.ToString("dd-MM-yyyy");
                dbconn.Insert(String.Format("INSERT INTO Rezervace (Uzivatel_ID, Film_ID, Platnost_do) " +
                    "SELECT Uzivatel_ID, Film_ID, str_to_date( '{2}', '%d-%m-%Y') " +
                    "FROM Uzivatel, Film " +
                    "WHERE Uzivatel.Prezdivka='{0}' AND Film.Nazev='{1}'", nickname, s[0], date));

                dbconn.Update(String.Format("UPDATE Film SET Rezervovany=1 WHERE Film.Nazev='{0}'", s[0]));

                refreshFilmy();
            }
        }
    }
}
