﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST2
{
    internal class Room
    {

        public string id { get; set; }
        public List<Bed> beds = new List<Bed>();
        public int maxWeight { get; set; }
        public int bedCount { get; set; }

        public Room(string id, int bedCount, int maxWeight)
        {
            this.id = id;
            this.bedCount = bedCount;
            this.maxWeight = maxWeight;

            for (int i = 0; i < bedCount; i++)
            {
                beds.Add(new Bed("L" + (i+1), maxWeight));
            }
        }

        public string RoomToString()
        {
            return "Bed count: " + bedCount;
        }

    }
}
