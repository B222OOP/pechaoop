﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPTEST2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Room> roomList = new List<Room>();
        List<Patient> patientList = new List<Patient>();

        public MainWindow()
        {
            InitializeComponent();
        }


        private void btnNewPatient_Click(object sender, RoutedEventArgs e)
        {

            PatientWindow PatientWindow = new PatientWindow();
            PatientWindow.ShowDialog();
            Patient patient = new Patient(PatientWindow.txtBxName.Text,
                                            PatientWindow.txtBxSurname.Text,
                                            int.Parse(PatientWindow.txtBxRc.Text),
                                            int.Parse(PatientWindow.txtBxWeight.Text),
                                            int.Parse(PatientWindow.txtBxHeight.Text));
            lBox3.Items.Add(patient.name);
            patientList.Add(patient);
        }

        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            RoomWindow RoomWindow = new RoomWindow();
            RoomWindow.ShowDialog();

            Room newRoom = new Room(RoomWindow.txtRoomId.Text, int.Parse(RoomWindow.txtNBeds.Text), int.Parse(RoomWindow.txtMaxW.Text));

            lBox1.Items.Add(newRoom.id);
            roomList.Add(newRoom);
        }

        private void lBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lBox2.SelectedIndex == -1)
            {
                return;
            }

            int selectedRoomId = lBox1.SelectedIndex;
            Room selectedRoom = roomList[selectedRoomId];

            int selectedBedId = lBox2.SelectedIndex;
            Bed selectedBed = selectedRoom.beds[selectedBedId];

            txtInfo.Text = selectedBed.BedToString();

        }

        private void btnMoveToRoom_Click(object sender, RoutedEventArgs e)
        {
            int selectedPatientId = lBox3.SelectedIndex;
            Patient selectedPatient = patientList[selectedPatientId];

            int selectedRoomId = lBox1.SelectedIndex;
            Room selectedRoom = roomList[selectedRoomId];

            for(int i = 0; i < selectedRoom.bedCount; i++)
            {
                if(!selectedRoom.beds[i].occupied)
                {
                    if (selectedPatient.weight <= selectedRoom.beds[i].maxWeight)
                    {
                        selectedRoom.beds[i].occupied = true;
                        selectedRoom.beds[i].occupiedBy = selectedPatient.name + " " + selectedPatient.surname;

                        txtErrorMessage.Text = selectedPatient.name + " " + selectedPatient.surname + " is now occupying bed " + selectedRoom.beds[i].id + ".";

                        return;
                    }
                    else { txtErrorMessage.Text = "Patient is too heavy."; }
                }
                
            }
            txtErrorMessage.Text = "There's no available bed in the room " + selectedRoom.id;


        }

        private void lBox1_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            lBox2.Items.Clear();
            lBox2.UnselectAll();
            txtInfo.Text = "";

            lBox2.Visibility = Visibility.Visible;

            int selectedRoomId = lBox1.SelectedIndex;
            Room selectedRoom = roomList[selectedRoomId];

            lbl2.Content = selectedRoom.id;

            for (int i = 0; i < selectedRoom.bedCount; i++)
            {
                lBox2.Items.Add(selectedRoom.beds[i].id);
            }
        }

        private void lBox3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedPatientId = lBox3.SelectedIndex;
            Patient selectedPatient = patientList[selectedPatientId];

            txtPatient_Info.Text = selectedPatient.PatientToString();
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            HelpWindow helpWindow = new HelpWindow();
            helpWindow.ShowDialog();
        }

        private void btnRemovePatient_Click(object sender, RoutedEventArgs e)
        {
            int selectedRoomId = lBox1.SelectedIndex;
            Room selectedRoom = roomList[selectedRoomId];

            int selectedBedId = lBox2.SelectedIndex;
            Bed selectedBed = selectedRoom.beds[selectedBedId];

            selectedBed.occupied = false;
            selectedBed.occupiedBy = null;

            txtErrorMessage.Text = "Bed " + selectedBed.id + " in room " + selectedRoom.id + " is now not occupied";
        }
    }
}
