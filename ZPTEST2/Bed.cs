﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST2
{
    internal class Bed
    {
        public string id { set; get; }
        public int maxWeight { set; get; }
        public bool occupied { set; get; }
        public string occupiedBy { set; get; }

        public Bed (string id, int maxWeight)
        {
            this.id = id;
            this.maxWeight = maxWeight;
            occupied = false;
            occupiedBy = "Free";
        }

        public string BedToString()
        {
            return "Max weight: " + maxWeight + "\nOccupied by: " + occupiedBy;
        }
    }
}
