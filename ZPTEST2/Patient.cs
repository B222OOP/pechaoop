﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST2
{
    public class Patient
    {
        public string name { set; get; }
        public string surname { set; get; }
        public int rc { set; get; }
        public int weight { set; get; }
        public int height { set; get; }

        public Patient (string name, string surname, int rc, int weight, int height)
        {
            this.name = name;
            this.surname = surname;
            this.rc = rc;
            this.weight = weight;
            this.height = height;
        }

        public string PatientToString()
        {
            return "Name: " + name 
                + "\nSurname: " + surname
                + "\nRC: " + rc
                + "\nWeight: " + weight
                + "\nHeight: " + height;
        }
    }
}
