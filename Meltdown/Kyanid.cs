﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Meltdown
{
    internal class Kyanid : Nadoba
    {
        public Kyanid()
        {
            maxQuantity = 200;
            actQuantity = 0;
            Color = System.Drawing.Color.Red;
        }
        public override int Napln(double cas)
        {
            actQuantity += 10;

            if(actQuantity > maxQuantity)
            {
                actQuantity = maxQuantity;
                return actQuantity;
            }

            return actQuantity;
        }

        public override bool plno()
        {
            if (actQuantity >= maxQuantity)
            {
                return true;
            }
            else return false;
        }
    }
}
