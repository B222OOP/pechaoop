﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meltdown
{
    internal abstract class Nadoba
    {
        public int maxQuantity { get; set; }
        public int actQuantity { get; set; }
        public Color Color { get; set; }

        public abstract int Napln(double cas);

        public abstract bool plno();
        public void vypustit()
        {
            actQuantity = 0;
        }
    }
}
