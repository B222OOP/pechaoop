﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Meltdown
{
    internal class Fenol : Nadoba
    {
        public int pritok { get; set; }
        
        public Fenol()
        {
            maxQuantity = 300;
            actQuantity = 0;
            pritok = 1;
            Color = System.Drawing.Color.Green;
        }
        public override int Napln(double cas)
        {
            actQuantity += pritok;
            pritok++;

            if (actQuantity > maxQuantity)
            {
                actQuantity = maxQuantity;
                return actQuantity;
            }

            return actQuantity;

        }

        public override bool plno()
        {
            if (actQuantity >= maxQuantity)
            {
                pritok = 0;
                return true;
            }
            else return false;
        }
    }
}
