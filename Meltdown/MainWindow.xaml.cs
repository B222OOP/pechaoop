﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Color = System.Drawing.Color;

namespace Meltdown
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Nadoba kyanid { get; set; }
        private Fenol fenol { get; set; }
        private DateTime TimerStart { get; set; }
        DispatcherTimer dispatcherTimer { get; set; }
        private string selection { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            kyanid = new Kyanid();
            fenol = new Fenol();

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
            this.TimerStart = DateTime.Now;

            kyanidQuantity.Fill = new SolidColorBrush(Colors.Green);
            fenolQuantity.Fill = new SolidColorBrush(Colors.Red);
        }
        public void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            var currentValue = DateTime.Now - TimerStart;
            double cas = int.Parse(currentValue.Seconds.ToString());


            kyanidQuantity.Height = kyanid.Napln(cas);
            fenolQuantity.Height = fenol.Napln(cas);

            kyanidState.Content = String.Format("Naplněno {0}/{1}", kyanid.actQuantity, kyanid.maxQuantity);
            fenolState.Content = String.Format("Naplněno {0}/{1}", fenol.actQuantity, fenol.maxQuantity);


            if (kyanid.plno() && fenol.plno())
            {
                dispatcherTimer.Stop();
            }

            if(kyanidQuantity.ActualHeight > 120 || fenolQuantity.ActualHeight > 200)
            {
                if (vystraha.IsChecked == true)
                {
                    lblWarn.Visibility = Visibility.Visible;
                }
                else
                {
                    lblWarn.Visibility = Visibility.Hidden;
                }
            }
        }

        private void btnVypustit_Click(object sender, RoutedEventArgs e)
        {
            if(selection == "kyanid")
            {
                dispatcherTimer.Stop();
                dispatcherTimer.Interval = new TimeSpan(0, 0, 1); 
                dispatcherTimer.Start();
                kyanid.vypustit();
            }
            else if(selection == "fenol")
            {
                dispatcherTimer.Stop();
                dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                dispatcherTimer.Start();
                fenol.pritok = 1;
                fenol.vypustit();
            }
        }

        private void kyanidSelection_Checked(object sender, RoutedEventArgs e)
        {
            selection = "kyanid";
        }

        private void fenolSelection_Checked(object sender, RoutedEventArgs e)
        {
            selection = "fenol";
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            string filepath = @"C:\\Users\\honzi\\Documents\\Gitlab\\pechaoop\\Meltdown\\stav.txt";
            string save = String.Format("Kyanid: {0} {1}\n Fenol:{2} {3}", kyanid.actQuantity,kyanid.maxQuantity, fenol.actQuantity, fenol.maxQuantity);
            File.WriteAllText(filepath, save);
        }

        private void loadBtn_Click(object sender, RoutedEventArgs e)
        {
            string filepath = @"C:\\Users\\honzi\\Documents\\Gitlab\\pechaoop\\Meltdown\\stav.txt";
            string readText = File.ReadAllText(filepath);
            string[] s = readText.Split(" ");

            kyanid.actQuantity = int.Parse(s[1]);
            fenol.actQuantity = int.Parse(s[4]);
            fenol.pritok = 1;
        }
    }
}
