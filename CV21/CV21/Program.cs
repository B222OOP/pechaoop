﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace CV21
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Ukladani vytvareni
            XDocument xdoc = new XDocument(
                new XElement("Androids",
                    new XElement("Android",
                        new XAttribute("code_name", "2B"),
                            new XElement("model_info",
                                new XAttribute("model", "B"),
                                new XAttribute("version", "2"))),
                    new XElement("Android",
                        new XAttribute("code_name", "9S"),
                            new XElement("model_info",
                                new XAttribute("model", "S"),
                                new XAttribute("version", "9")))));
            string filepath = @"C:\Users\honzi\Documents\Gitlab\pechaoop\CV21\androids.xml";
            xdoc.Save(filepath);


            // Nacteni XML
            XDocument loadedxdoc = XDocument.Load(filepath);

            foreach(var node in loadedxdoc.DescendantNodes())
            {
                Console.WriteLine(
                    String.Format("V promenne se nachazi:{0} a je to typu:{1} ", node, node.GetType()));
                if (node is XText)
                {

                }
            }

            var q = loadedxdoc.Root.Elements().Where(a => a.Element("code_name") != null &&
                                      a.Element("code_name").Value == "2B")
                           .OrderBy(a => a.Element("code_name").Value)  
                           .Select(a => a.Element("version").Value);

            Console.ReadKey();
        }
    }
}
