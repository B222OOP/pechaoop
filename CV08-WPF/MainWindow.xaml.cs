﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CV08_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point position;
        Random rnd = new Random();
        private Rectangle rct;
        private DispatcherTimer timer;
        private int smer = 1; // 1 doprava, -1 doleva

        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(dispatcherTimer_Tick);
            timer.Interval = TimeSpan.FromMilliseconds(10);
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (this.position.X + this.rct.Width < cnv.Width || this.position.X - this.rct.Width < 0 + this.rct.Width)
            {
                this.smer *= -1;
            }
            this.position.X += (5 * smer);

            Canvas.SetLeft(rct, position.X);
        }

        public void addRectangle(int n)
        {

            for (int i = 0; i < n; i++)
            {

                // Vytvoreni rectangle
                this.rct = new Rectangle();
                rct.Fill = new SolidColorBrush(Colors.BlueViolet);
                rct.Width = 20;
                rct.Height = 40;

                this.position = new Point();
                this.position.X = rnd.Next((int)cnv.Margin.Left, 600 - (int)rct.Width);
                this.position.Y = rnd.Next((int)cnv.Margin.Top, 260 - (int)rct.Height);

                //Pozici canvasu
                Canvas.SetLeft(rct, this.position.X);
                Canvas.SetTop(rct, this.position.Y);

                //Zarazeni do canvasu
                cnv.Children.Add(rct);

            }


        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            addRectangle(int.Parse(txtbox.Text));
            timer.Start();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            cnv.Children.Clear();
        }
    }
}
